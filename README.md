# Taller: Ordenamiento de Documentos con Merge Sort

## Objetivo:
El objetivo de este taller es implementar el algoritmo de ordenamiento Merge Sort para organizar al menos 500 documentos dentro de un array. Esto se llevará a cabo mediante la programación en pares y haciendo uso de herramientas como GitHub Copilot, Easy Code o Phind. Luego, se procederá a documentar el laboratorio en un informe con formato IEEE y LaTeX, incluyendo el análisis de la complejidad algorítmica.

## Parte 1: Implementación del Algoritmo

### 1.1 Implementación del Algoritmo

Utilice una herramienta de desarrollo como GitHub Copilot, Easy Code o Phind para implementar el algoritmo de ordenamiento Merge Sort en el lenguaje de programación de su elección.

### 1.2 Carga de Documentos

Cree un programa que cargue al menos 500 documentos en un array. Estos documentos pueden ser nombres de archivos, títulos de documentos u otra información relevante. Puede generar datos aleatorios o utilizar datos reales si están disponibles.

### 1.3 Aplicación de Merge Sort

Aplique el algoritmo Merge Sort para ordenar los documentos almacenados en el array. La ordenación se realizará en orden alfabético o según un criterio relevante.

## Parte 2: Documentación en Formato IEEE y LaTeX

### 2.1 Creación del Informe

Elabore un informe siguiendo el formato IEEE utilizando LaTeX. Puede utilizar una plantilla IEEE existente o crear una desde cero.

### 2.2 Documentación del Algoritmo

Documente el proceso de implementación del algoritmo Merge Sort en su programa. Proporcione una explicación detallada de cada paso clave e incluya ejemplos de código.

### 2.3 Carga de Documentos

Describa cómo se seleccionaron y cargaron los documentos en el array. Explique si los datos se generaron aleatoriamente o se obtuvieron de fuentes reales.

### 2.4 Explicación de Merge Sort

Incluya una sección en la que se explique en detalle el funcionamiento del algoritmo Merge Sort aplicado a la ordenación de los documentos en el array.

### 2.5 Análisis de Complejidad

Realice un análisis de la complejidad algorítmica del algoritmo Merge Sort en términos de tiempo y espacio. Explique cómo esta complejidad se relaciona con el tamaño de los datos, en este caso, la cantidad de documentos.

## Parte 3: Comprobación y Evaluación

### 3.1 Comprobación de Resultados

Ejecute el programa para ordenar los documentos y verifique que los resultados sean correctos. Asegúrese de que los documentos estén ordenados según el criterio especificado.

### 3.2 Evaluación de Eficiencia

Realice una evaluación de la eficiencia y velocidad del algoritmo de Merge Sort en función de la cantidad de documentos procesados. Considere cómo el rendimiento del algoritmo varía con diferentes tamaños de entrada.

## Parte 4: Repositorio y Entrega

### 4.1 Creación de Repositorio

Cree un repositorio donde almacenará el código fuente de su implementación, el informe en formato IEEE y cualquier otro recurso relevante.

### 4.2 Entrega

Proporcione el enlace al repositorio y el informe en formato PDF para su entrega.

